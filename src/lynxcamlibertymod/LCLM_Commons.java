package lynxcamlibertymod;

import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LCLM_Commons
{
    public static ArrayList<String> imposters = new ArrayList<>();
    
    public static ItemStack getBanHammer()
    {
        ItemStack banhammer = new ItemStack(Material.DIAMOND_AXE, 1);
        ItemMeta banhammermeta = banhammer.getItemMeta();
        banhammermeta.setLore(Arrays.asList(ChatColor.BLUE + "Unleash the power of...", ChatColor.YELLOW + "The BanHammer!"));
        banhammermeta.setDisplayName(ChatColor.RED + "BanHammer!");
        banhammer.setItemMeta(banhammermeta);
        return banhammer;
    }
    
    public static void playerMsg(CommandSender sender, String msg, ChatColor colour)
    {
        sender.sendMessage(colour + msg);
    }
    
    public static void playerMsg(CommandSender sender, String msg)
    {
        playerMsg(sender, msg, ChatColor.RED);
    }
   
    public static void adminAction(String name, String msg, boolean isRed)
    {
        Bukkit.broadcastMessage((isRed ? ChatColor.RED : ChatColor.DARK_AQUA) + name + " - " + msg);
    }
}
