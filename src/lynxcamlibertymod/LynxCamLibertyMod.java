package lynxcamlibertymod;

import java.util.logging.Level;
import lynxcamlibertymod.commands.LCLM_CommandRegistry;
import lynxcamlibertymod.listeners.LCLM_CamVerifyListener;
import lynxcamlibertymod.listeners.LCLM_PlayerListener;
import lynxcamlibertymod.listeners.LCLM_TelnetListener;
import lynxcamlibertymod.listeners.LCLM_TogglableEventsListener;
import lynxcamlibertymod.totalfreedoworld.TFM_AdminWorld;
import lynxcamlibertymod.totalfreedoworld.TFM_Flatlands;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class LynxCamLibertyMod extends JavaPlugin
{

    public static LynxCamLibertyMod plugin;

    @Override
    public void onEnable()
    {
        plugin = this;
        PluginDescriptionFile pdf = this.getDescription();
        getLogger().log(Level.INFO, "{0}{1} v. {2} by {3} has been enabled!", new Object[]
        {
            ChatColor.BLUE, pdf.getName(), pdf.getVersion(), pdf.getAuthors()
        });
        new LCLM_Configs();
        new LCLM_CommandRegistry();
        new LCLM_PlayerListener();
        new LCLM_TelnetListener();
        new LCLM_CamVerifyListener();
        new LCLM_TogglableEventsListener();
        for (Player player : Bukkit.getOnlinePlayers())
        {
            FileConfiguration config = LCLM_Configs.admins.getConfig();
            if (config.getBoolean(player.getUniqueId().toString() + ".imposter"))
            {
                LCLM_Commons.imposters.add(player.getName());
            }
        }
        try
        {
            TFM_Flatlands.getInstance().getWorld();
        }
        catch (Exception ex)
        {
            Bukkit.broadcastMessage("Could not load world: Flatlands");
        }

        try
        {
            TFM_AdminWorld.getInstance().getWorld();
        }
        catch (Exception ex)
        {
            Bukkit.broadcastMessage("Could not load world: AdminWorld");
        }
    }

    @Override
    public void onDisable()
    {
        PluginDescriptionFile pdf = this.getDescription();
        getLogger().log(Level.INFO, "{0}{1} has been disabled!", new Object[]
        {
            ChatColor.RED, pdf.getName()
        });
    }
}
