package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import net.camtech.camutils.CUtils_Methods;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_kick extends LCLM_Command
{

    public Command_kick()
    {
        super("kick", "/kick [player] <reason>", "Kick a player.", Rank.ADMIN);
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length < 1)
        {
            return false;
        }
        if(args.length == 1)
        {
            Player player = Bukkit.getPlayer(args[0]);
            if(player == null)
            {
                sender.sendMessage("The player " + args[0] + " is not online.");
                return true;
            }
            if(LCLM_Rank.getRank(sender).level > LCLM_Rank.getRank(player).level)
            {
                player.kickPlayer("You have been kicked by " + sender.getName());
                return true;
            }
            sender.sendMessage(ChatColor.RED + "You cannot kick someone of an equal or higher rank than yourself.");
            return true;
        }
        else
        {
            String message = CUtils_Methods.colour(StringUtils.join(args, " ", 1));
            Player player = Bukkit.getPlayer(args[0]);
            if(player == null)
            {
                sender.sendMessage("The player " + args[0] + " is not online.");
                return true;
            }
            if(LCLM_Rank.getRank(sender).level > LCLM_Rank.getRank(player).level)
            {
                player.kickPlayer(message + " - " + sender.getName());
                return true;
            }
            sender.sendMessage(ChatColor.RED + "You cannot kick someone of an equal or higher rank than yourself.");
            return true;
        }
    }
    
}
