package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Rank.Rank;
import lynxcamlibertymod.LynxCamLibertyMod;
import lynxcamlibertymod.totalfreedoworld.TFM_Flatlands;
import net.camtech.camutils.CUtils_Methods;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

public class Command_wipeflatlands extends LCLM_Command
{

    public Command_wipeflatlands()
    {
        super("wipeflatlands", "/wipeflatlands", "Wipe the flatlands.", Rank.SPECIALIST);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        World flatlands = Bukkit.getWorld("flatlands");
        CUtils_Methods.unloadWorld(flatlands);
        CUtils_Methods.deleteWorld(flatlands.getWorldFolder());
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                try
                {
                    TFM_Flatlands.getInstance().getWorld();
                }
                catch (Exception ex)
                {
                    Bukkit.broadcastMessage("Could not load world: Flatlands");
                }
            }
        }.runTaskLater(LynxCamLibertyMod.plugin, 20L * 5L);
        Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " - Wiping flatlands.");
        return true;
    }

}
