package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_chatlevel extends LCLM_Command
{

    public Command_chatlevel()
    {
        super("chatlevel", "/chatlevel [level]", "Change your chat level.", Arrays.asList("cl"), Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length < 1)
        {
            return false;
        }
        if (!(sender instanceof Player))
        {
            return true;
        }
        Player player = (Player) sender;
        int level = 0;
        try
        {
            level = Integer.parseInt(args[0]);
        }
        catch (Exception ex)
        {
            level = LCLM_Rank.getFromName(StringUtils.join(ArrayUtils.subarray(args, 0, args.length), " ")).level;
        }
        if (LCLM_Rank.getRank(sender).level >= level)
        {
            LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".chatLevel", level);
            sender.sendMessage(ChatColor.GREEN + "You are now talking in " + LCLM_Rank.getFromLevel(level).name + " Chat.");
        }
        else
        {
            sender.sendMessage(ChatColor.RED + "You do not have permission to access this chat level!");
        }
        return true;
    }

}
