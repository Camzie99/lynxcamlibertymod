package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Configs;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_djump extends LCLM_Command
{
    public Command_djump()
    {
        super("djump", "/djump", "Toggle your double jumping ability.", Arrays.asList("doublejump"));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage("This can only be used in game.");
            return true;
        }
        Player player = (Player) sender;
        sender.sendMessage(ChatColor.GREEN + "Toggled double jump mode.");
        LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".djump", !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".djump"));
        LCLM_Configs.getAdmins().saveConfig();
        return true;
    }
    
}
