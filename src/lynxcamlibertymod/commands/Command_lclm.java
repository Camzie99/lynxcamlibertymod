package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import net.camtech.camutils.CUtils_Methods;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Command_lclm extends LCLM_Command
{
    public Command_lclm()
    {
        super("lclm", "lclm [reload]", "Check information about the LCLM or reload the plugin's configuration files.");
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length == 1)
        {
            if(args[0].equalsIgnoreCase("reload"))
            {
                if(!LCLM_Rank.isAdmin(sender))
                {
                    sender.sendMessage("Only admins can reload the LCLM configuration files.");
                    return true;
                }
                LCLM_Configs.getAdmins().reloadConfig();
                LCLM_Configs.getBans().reloadConfig();
                LCLM_Configs.getCommands().reloadConfig();
                LCLM_Configs.getMainConfig().reloadConfig();
                sender.sendMessage(ChatColor.GREEN + "LCLM Configs reloaded!");
                return true;
            }
            return false;
        }
        else
        {
            sender.sendMessage(ChatColor.GREEN + "This is the LynxCamLibertyMod.");
            sender.sendMessage(CUtils_Methods.randomChatColour() + "an all new form of All-Op management.");
            sender.sendMessage(CUtils_Methods.colour("&-Created in the likes of the TFM but with more " + CUtils_Methods.randomChatColour() + "flexibility&-!"));
        }
        return true;
    }
    
}
