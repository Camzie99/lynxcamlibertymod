package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_fr extends LCLM_Command
{

    public Command_fr()
    {
        super("fr", "/fr <player>", "Freeze a player.", Arrays.asList("freeze", "halt"), Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length == 0)
        {
            Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + " - Toggling freeze over all players.");
            for (Player player : Bukkit.getOnlinePlayers())
            {
                LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".freeze", !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".freeze"));
                player.sendMessage(ChatColor.AQUA + "You are now frozen.");
                LCLM_Configs.getAdmins().saveConfig();
            }
        }
        if (args.length == 1)
        {
            Player player = Bukkit.getPlayer(args[0]);
            if (player == null)
            {
                sender.sendMessage("Player is not online.");
                return true;
            }
            Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + " - Toggling freeze over " + player.getName() + ".");
            LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".freeze", !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".freeze"));
            player.sendMessage(ChatColor.AQUA + "You are now frozen.");
            LCLM_Configs.getAdmins().saveConfig();
        }
        return false;
    }

}
