package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_blockcommand extends LCLM_Command
{
    public Command_blockcommand()
    {
        super("blockcommand", "/blockcommand [player]", "Block a player's commands.", Arrays.asList("blockcmd"), Rank.ADMIN);
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        Player player = Bukkit.getPlayer(args[0]);
        if(player == null)
        {
            sender.sendMessage("Player is not online.");
            return true;
        }
        if(LCLM_Rank.isEqualOrHigher(LCLM_Rank.getRank(player), LCLM_Rank.getRank(sender)) && !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".cmdblock"))
        {
            sender.sendMessage("You can only block the commands of a player with lower clearance than yourself.");
            return true;
        }
        Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + " - toggling command blockage for " + player.getName() + ".");
        LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".cmdblock", !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".cmdblock"));
        LCLM_Configs.getAdmins().saveConfig();
        return true;
    }
    
}
