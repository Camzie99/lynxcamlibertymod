package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_deopall extends LCLM_Command
{
    public Command_deopall()
    {
        super("deopall", "/deopall", "Deop all players on the server.", Rank.SUPER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!LCLM_Rank.isSuper(sender))
        {
            sender.sendMessage(ChatColor.RED + "You don't have permissions to execute this command.");
            return true;
        }
        
        for(Player player : Bukkit.getOnlinePlayers())
        {
            if(!LCLM_Rank.isAdmin(player))
            {
                player.setOp(false);
            }
        }
        return true;
    } 
    
}
