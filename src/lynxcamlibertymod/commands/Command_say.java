package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Rank.Rank;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Command_say extends LCLM_Command
{
    
    public Command_say()
    {
        super("say", "/say [message]", "Broadcast a message to the server.", "You don't have permission to use this command.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length == 0)
        {
            return false;
        }
        String message = StringUtils.join(args, " ");
        Bukkit.broadcastMessage(ChatColor.DARK_AQUA + "[" + sender.getName() + "] " + ChatColor.ITALIC + message);
        return true;
    }
    
}
