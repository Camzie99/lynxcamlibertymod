package lynxcamlibertymod.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_flatlands extends LCLM_Command
{
    public Command_flatlands()
    {
        super("flatlands", "/flatlands", "Go to the flatlands.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage(ChatColor.RED + "You must be ingame to use this command.");
            return true;
        }
        if(((Player) sender).getWorld() == Bukkit.getWorld("flatlands"))
        {
            ((Player) sender).teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
            return true;
        }
        try
        {
            ((Player) sender).teleport(Bukkit.getWorld("flatlands").getSpawnLocation());
        }
        catch(Exception ex)
        {
            sender.sendMessage("The flatlands cannot be found, they are likely in the middle of a wipe, please try again in a few seconds.");
        }
        return true;
    }
    
}
