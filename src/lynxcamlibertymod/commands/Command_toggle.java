package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank.Rank;
import lynxcamlibertymod.listeners.LCLM_TogglableEventsListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Command_toggle extends LCLM_Command
{
    public Command_toggle()
    {
        super("toggle", "/toggle [value]", "Toggle a server setting.", Rank.EXECUTIVE);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length == 0)
        {
            sender.sendMessage(ChatColor.AQUA + "Possible toggles:");
            for(String toggle : LCLM_Configs.getMainConfig().getConfig().getConfigurationSection("toggles").getKeys(false))
            {
                sender.sendMessage(" - " + (LCLM_Configs.getMainConfig().getConfig().getBoolean("toggles." + toggle) ? ChatColor.GREEN : ChatColor.RED) + toggle);
            }
            return true;
        }
        if(args.length == 1)
        {
            for(String toggle : LCLM_Configs.getMainConfig().getConfig().getConfigurationSection("toggles").getKeys(false))
            {
                if(args[0].equalsIgnoreCase(toggle))
                {
                    LCLM_Configs.getMainConfig().getConfig().set("toggles." + toggle, !LCLM_Configs.getMainConfig().getConfig().getBoolean("toggles." + toggle));
                    LCLM_Configs.getMainConfig().saveConfig();
                    sender.sendMessage(ChatColor.GOLD + "Toggled " + toggle + (LCLM_Configs.getMainConfig().getConfig().getBoolean("toggles." + toggle) ? " on." : " off."));
                    LCLM_TogglableEventsListener.checkTime();
                    return true;
                }
            }
            sender.sendMessage(ChatColor.AQUA + "Possible toggles:");
            for(String toggle : LCLM_Configs.getMainConfig().getConfig().getConfigurationSection("toggles").getKeys(false))
            {
                sender.sendMessage(" - " + (LCLM_Configs.getMainConfig().getConfig().getBoolean("toggles." + toggle) ? ChatColor.GREEN : ChatColor.RED) + toggle);
            }
            return true;
        }
        return false;
    }
    
}
