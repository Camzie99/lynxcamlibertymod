package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import lynxcamlibertymod.totalfreedoworld.TFM_AdminWorld;
import static me.StevenLawson.BukkitTelnet.BukkitTelnet.server;
import org.bukkit.Bukkit;
import static org.bukkit.Bukkit.getPlayer;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_adminworld extends LCLM_Command
{

    public Command_adminworld()
    {
        super("adminworld", "command> [guest < list | purge | add <player> | remove <player> > | time <morning | noon | evening | night> | weather <off | on | storm>]", "", Rank.ADMIN);
    }

    private enum CommandMode
    {

        TELEPORT, GUEST, TIME, WEATHER;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
    {

        CommandMode commandMode = null;

        if (args.length == 0)
        {
            commandMode = CommandMode.TELEPORT;
        }
        else if (args.length >= 2)
        {
            if ("guest".equalsIgnoreCase(args[0]))
            {
                commandMode = CommandMode.GUEST;
            }
            else if ("time".equalsIgnoreCase(args[0]))
            {
                commandMode = CommandMode.TIME;
            }
            else if ("weather".equalsIgnoreCase(args[0]))
            {
                commandMode = CommandMode.WEATHER;
            }
        }

        if (commandMode == null)
        {
            return false;
        }

        if (!(sender instanceof Player))
        {
            return true;
        }
        Player sender_p = (Player) sender;

        try
        {
            switch (commandMode)
            {
                case TELEPORT:
                {

                    World adminWorld = null;
                    try
                    {
                        adminWorld = TFM_AdminWorld.getInstance().getWorld();
                    }
                    catch (Exception ex)
                    {
                    }

                    if (adminWorld == null || sender_p.getWorld() == adminWorld)
                    {
                        sender.sendMessage("Going to the main world.");
                        sender_p.teleport(server.getWorlds().get(0).getSpawnLocation());
                    }
                    else
                    {
                        if (TFM_AdminWorld.getInstance().canAccessWorld(sender_p))
                        {
                            sender.sendMessage("Going to the AdminWorld.");
                            TFM_AdminWorld.getInstance().sendToWorld(sender_p);
                        }
                        else
                        {
                            sender.sendMessage("You don't have permission to access the AdminWorld.");
                        }
                    }

                    break;
                }
                case GUEST:
                {
                    if (args.length == 2)
                    {
                        if ("list".equalsIgnoreCase(args[1]))
                        {
                            sender.sendMessage("AdminWorld guest list: " + TFM_AdminWorld.getInstance().guestListToString());
                        }
                        else if ("purge".equalsIgnoreCase(args[1]))
                        {
                            assertCommandPerms(sender, sender_p);
                            TFM_AdminWorld.getInstance().purgeGuestList();
                            Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + "AdminWorld guest list purged.");
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else if (args.length == 3)
                    {
                        assertCommandPerms(sender, sender_p);

                        if ("add".equalsIgnoreCase(args[1]))
                        {
                            final Player player = getPlayer(args[2]);

                            if (player == null)
                            {
                                sender.sendMessage("Player not found...");
                                return true;
                            }

                            if (TFM_AdminWorld.getInstance().addGuest(player, sender_p))
                            {
                                Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + "AdminWorld guest added: " + player.getName());
                            }
                            else
                            {
                                sender.sendMessage("Could not add player to guest list.");
                            }
                        }
                        else if ("remove".equals(args[1]))
                        {
                            final Player player = TFM_AdminWorld.getInstance().removeGuest(args[2]);
                            if (player != null)
                            {
                                Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + "AdminWorld guest removed: " + player.getName());
                            }
                            else
                            {
                                sender.sendMessage("Can't find guest entry for: " + args[2]);
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }

                    break;
                }
                case TIME:
                {
                    assertCommandPerms(sender, sender_p);

                    if (args.length == 2)
                    {
                        TFM_AdminWorld.TimeOfDay timeOfDay = TFM_AdminWorld.TimeOfDay.getByAlias(args[1]);
                        if (timeOfDay != null)
                        {
                            TFM_AdminWorld.getInstance().setTimeOfDay(timeOfDay);
                            sender.sendMessage("AdminWorld time set to: " + timeOfDay.name());
                        }
                        else
                        {
                            sender.sendMessage("Invalid time of day. Can be: sunrise, noon, sunset, midnight");
                        }
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }
                case WEATHER:
                {
                    assertCommandPerms(sender, sender_p);

                    if (args.length == 2)
                    {
                        TFM_AdminWorld.WeatherMode weatherMode = TFM_AdminWorld.WeatherMode.getByAlias(args[1]);
                        if (weatherMode != null)
                        {
                            TFM_AdminWorld.getInstance().setWeatherMode(weatherMode);
                            sender.sendMessage("AdminWorld weather set to: " + weatherMode.name());
                        }
                        else
                        {
                            sender.sendMessage("Invalid weather mode. Can be: off, rain, storm");
                        }
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }
                default:
                {
                    return false;
                }
            }
        }
        catch (PermissionDeniedException ex)
        {
            sender.sendMessage(ex.getMessage());
        }

        return true;
    }

    private void assertCommandPerms(CommandSender sender, Player sender_p) throws PermissionDeniedException
    {
        if (!(sender instanceof Player) || sender_p == null || !LCLM_Rank.isAdmin(sender))
        {
            throw new PermissionDeniedException("You don't have permission to use this command.");
        }
    }

    private class PermissionDeniedException extends Exception
    {

        private static final long serialVersionUID = 1L;

        private PermissionDeniedException(String string)
        {
            super(string);
        }
    }

}
