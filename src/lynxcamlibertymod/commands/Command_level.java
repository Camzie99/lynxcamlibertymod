package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Rank;
import net.camtech.camutils.CUtils_Methods;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class Command_level extends LCLM_Command
{

    public Command_level()
    {
        super("level", "/level", "See what clearence level you have.", "You aren't allowed to use this command.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        sender.sendMessage(ChatColor.GREEN + "You have level " + ChatColor.BLUE + Integer.toString(LCLM_Rank.getRank(sender).level) + ChatColor.GREEN + " clearance as " + CUtils_Methods.aOrAn(LCLM_Rank.getRank(sender).name) + " " + LCLM_Rank.getRank(sender).name + ".");
        return true;
    }
    
}
