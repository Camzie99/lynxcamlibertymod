package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Bans;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_ban extends LCLM_Command
{

    public Command_ban()
    {
        super("ban", "/ban [player] <reason>", "Ban a player.", "You don't have permission to use this command.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (args.length < 1)
        {
            return false;
        }
        final Player player = Bukkit.getPlayer(args[0]);
        String reason = "No reason given... - " + sender.getName();
        if (args.length > 1)
        {
            reason = StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ") + " - " + sender.getName();
        }
        if (player == null)
        {
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Attempting ban of offline player: " + args[0]);
            LCLM_Bans.addBan(args[0], reason);
        }
        else
        {
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Attempting to ban player: " + player.getName());
            LCLM_Bans.addBan(player, reason);
        }
        return true;
    }

}
