package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Rank.Rank;
import net.camtech.camutils.CUtils_Methods;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Command_cookie extends LCLM_Command
{
    public Command_cookie()
    {
        super("cookie", "/cookie", "Give everyone a cookie.", Rank.SUPER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        for(Player player : Bukkit.getOnlinePlayers())
        {
            Bukkit.broadcastMessage(CUtils_Methods.randomChatColour() + sender.getName() + " - giving all players a " + CUtils_Methods.randomChatColour() + "COOKIE!");
            ItemStack cookie = new ItemStack(Material.COOKIE, 1);
            ItemMeta cookiemeta = cookie.getItemMeta();
            cookiemeta.setLore(Arrays.asList(CUtils_Methods.randomChatColour() + "A cookie gift :)", CUtils_Methods.randomChatColour() + " - " + sender.getName()));
            cookiemeta.setDisplayName(CUtils_Methods.randomChatColour() + "Cookie!");
            cookie.setItemMeta(cookiemeta);
            player.getInventory().addItem(cookie);
        }
        return true;
    }
    
}
