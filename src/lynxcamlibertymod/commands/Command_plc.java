package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

public class Command_plc extends LCLM_Command
{
    public Command_plc()
    {
        super("plc", "/plc [enable | disable | reload] [plugin name]", "Manage plugins.", Arrays.asList("plugincontrol"), Rank.EXECUTIVE);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 2)
        {
            return false;
        }
        PluginManager manager = Bukkit.getPluginManager();
        Plugin plugin = manager.getPlugin(args[1]);
        if(plugin == null)
        {
            sender.sendMessage(args[1] + " is not a valid plugin...");
            return true;
        }
        if(args[1].equalsIgnoreCase("Essentials"))
        {
            sender.sendMessage("Essentials cannot be managed at this time.");
            return true;
        }
        switch(args[0].toLowerCase())
        {
            case "enable":
                if(manager.isPluginEnabled(plugin))
                {
                    sender.sendMessage("Plugin is already enabled.");
                    break;
                }
                manager.enablePlugin(plugin);
                LCLM_CommandRegistry.registerCommands();
                break; 
            case "disable":
                if(!manager.isPluginEnabled(plugin))
                {
                    sender.sendMessage("Plugin is already disabled.");
                    break;
                }
                manager.disablePlugin(plugin);
                LCLM_CommandRegistry.registerCommands();
                break;
            case "reload":
                manager.disablePlugin(plugin);
                manager.enablePlugin(plugin);
                LCLM_CommandRegistry.registerCommands();
                break;
            default:
                return false;
        }
        sender.sendMessage(ChatColor.GREEN + "Action " + ChatColor.AQUA + args[0].toLowerCase() + ChatColor.GREEN + " performed on " + ChatColor.AQUA + plugin.getName() + ChatColor.GREEN + " successfully.");
        return true;
    }
}
