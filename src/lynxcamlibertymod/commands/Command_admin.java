package lynxcamlibertymod.commands;

import java.util.ArrayList;
import java.util.Set;
import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import static lynxcamlibertymod.LCLM_Rank.isSenior;
import net.camtech.camutils.CUtils_Methods;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

@CommandParameters(name="admin", usage="/admin [[add | delete] [username] <rank>] | [list]", description="Add somebody to admin.")
public class Command_admin
{

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        FileConfiguration admins = LCLM_Configs.getAdmins().getConfig();
        if (args.length == 1)
        {
            if(args[0].equalsIgnoreCase("list"))
            {
                ArrayList<String> adminlist = new ArrayList<>();
                for(String name : (Set<String>) admins.getConfigurationSection("").getKeys(false))
                {
                    if(!admins.getString(name + ".rank").equalsIgnoreCase("op"))
                    {
                        adminlist.add(CUtils_Methods.colour(LCLM_Rank.getFromName(admins.getString(name + ".rank")).tag + " " + ChatColor.GOLD + admins.getString(name + ".lastName")));
                    }
                }
                sender.sendMessage(ChatColor.AQUA + "LCLM Admins:");
                sender.sendMessage("    " + StringUtils.join(adminlist, ", "));
                return true;
            }
            return false;
        }
        if(!isSenior(sender))
        {
            return true;
        }
        Player player;
        if (args.length == 2)
        {
            player = Bukkit.getPlayer(args[1]);
            if (player == null)
            {
                sender.sendMessage("Player: " + args[1] + " is not online.");
                return true;
            }
            if (args[0].equalsIgnoreCase("delete"))
            {
                if (LCLM_Rank.isEqualOrHigher(LCLM_Rank.getRank(player), LCLM_Rank.getRank(sender)))
                {
                    sender.sendMessage("You can only remove someone of a lower rank than yourself from admin.");
                    return true;
                }
                LCLM_Rank.setRank(player, LCLM_Rank.Rank.OP, sender);
                return true;
            }
            if (args[0].equalsIgnoreCase("add"))
            {
                LCLM_Rank.setRank(player, LCLM_Rank.Rank.ADMIN, sender);
                return true;
            }
            return false;
        }
        if (args.length >= 3)
        {
            if (!args[0].equalsIgnoreCase("add"))
            {
                sender.sendMessage("You only need 2 arguments for a removal.");
                return true;
            }
            player = Bukkit.getPlayer(args[1]);
            if (player == null)
            {
                sender.sendMessage("Player: " + args[1] + " is not online.");
                return true;
            }
            int level = 0;
            try
            {
                level = Integer.parseInt(args[2]);
            }
            catch (Exception ex)
            {
                String rank = StringUtils.join(ArrayUtils.subarray(args, 2, args.length), " ");
                level = LCLM_Rank.getFromName(rank).level;
            }
            if (level == 0)
            {
                Bukkit.broadcastMessage(StringUtils.join(ArrayUtils.subarray(args, 2, args.length), " ") + " is an invalid rank.");
                return true;
            }
            LCLM_Rank.setRank(player, LCLM_Rank.getFromLevel(level), sender);
            return true;
        }
        return false;
    }

}
