package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import net.camtech.camutils.CUtils_Methods;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_tag extends LCLM_Command
{
    
    public Command_tag()
    {
        super("tag", "/tag [tag]");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length == 0)
        {
            return false;
        }
        if(!(sender instanceof Player))
        {
            sender.sendMessage("This can only be used in-game");
            return true;
        }
        String tag = StringUtils.join(args, " ");
        for(Rank rank : Rank.values())
        {
            if(tag.toLowerCase().contains(rank.name.toLowerCase()) && LCLM_Rank.getRank(sender).level < rank.level)
            {
                sender.sendMessage(ChatColor.RED + tag + " contains the name of a rank higher than yourself.");
                return true;
            }
        }
        Player player = (Player) sender;
        player.sendMessage(ChatColor.GREEN + "Tag set to " + CUtils_Methods.colour(tag));
        LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".tag", tag);
        LCLM_Configs.getAdmins().saveConfig();
        return true;
    }
    
}
