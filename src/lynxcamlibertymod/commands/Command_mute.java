package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_mute extends LCLM_Command
{
    public Command_mute()
    {
        super("mute", "/mute [player]", "Mute a player.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        Player player = Bukkit.getPlayer(args[0]);
        if(player == null)
        {
            sender.sendMessage("Player is not online.");
            return true;
        }
        if(LCLM_Rank.isEqualOrHigher(LCLM_Rank.getRank(player), LCLM_Rank.getRank(sender)) && !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".muted"))
        {
            sender.sendMessage("You can only mute someone of a lower rank than yourself.");
            return true;
        }   
        Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + " toggling mute for " + player.getName());
        LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".muted", !LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".muted"));
        LCLM_Configs.getAdmins().saveConfig();
        return true;
    }
    
}
