package lynxcamlibertymod.commands;

import java.util.Arrays;
import lynxcamlibertymod.LCLM_Configs;
import net.camtech.camutils.CUtils_Methods;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_nick extends LCLM_Command
{

    public Command_nick()
    {
        super("nick", "/nick [name]", "Give yourself a custom nickname.", Arrays.asList("nickname", "name"));
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length == 0)
            return false;   
        if(!(sender instanceof Player))
        {
            sender.sendMessage("This can only be used in-game.");
            return true;
        }
        Player player = (Player) sender;
        LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".displayName", StringUtils.join(args, " ") + "&r");
        LCLM_Configs.getAdmins().saveConfig();
        player.sendMessage(ChatColor.GREEN + "Nick set to " + CUtils_Methods.colour(StringUtils.join(args, " ")));
        return true;
    }
    
}
