package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Commons;
import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_banhammer extends LCLM_Command
{

    public Command_banhammer()
    {
        super("banhammer", "/banhammer", "Unleash the banhammer...", Rank.SPECIALIST);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        Player player = (Player) sender;
        if (LCLM_Configs.getAdmins().getConfig().getBoolean(player.getUniqueId().toString() + ".banHammer"))
        {
            player.getInventory().remove(LCLM_Commons.getBanHammer());
            LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".banHammer", false);
            Bukkit.broadcastMessage(ChatColor.AQUA + player.getName() + " has placed the BanHammer back into its sheath");
            LCLM_Configs.getAdmins().saveConfig();
            return true;
        }
        player.getInventory().addItem(LCLM_Commons.getBanHammer());
        player.getWorld().strikeLightning(player.getLocation());
        Bukkit.broadcastMessage(ChatColor.RED + player.getName() + " has unleashed the BanHammer!");
        LCLM_Configs.getAdmins().getConfig().set(player.getUniqueId().toString() + ".banHammer", true);
        LCLM_Configs.getAdmins().saveConfig();
        return true;
    }
}
