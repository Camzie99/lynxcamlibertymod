package lynxcamlibertymod.commands;

import lynxcamlibertymod.LCLM_Bans;
import lynxcamlibertymod.LCLM_Rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_unban extends LCLM_Command
{
    
    public Command_unban()
    {
        super("unban", "/unban [player]", "Unan a player.", "You don't have permission to use this command.", Rank.ADMIN);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length != 1)
        {
            return false;
        }
        Player player = Bukkit.getPlayer(args[0]);
        if(player == null)
        {
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Attempting unban of offline player: " + args[0]);
            LCLM_Bans.unBan(args[0]);
        }
        else
        {
            Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " - Attempting to unban player: " + player.getName());
            LCLM_Bans.unBan(player);
        }
        return true;
    }
    
}
