package lynxcamlibertymod.listeners;

import lynxcamlibertymod.LCLM_Configs;
import lynxcamlibertymod.LCLM_Rank;
import lynxcamlibertymod.LynxCamLibertyMod;
import me.StevenLawson.BukkitTelnet.api.TelnetCommandEvent;
import me.StevenLawson.BukkitTelnet.api.TelnetPreLoginEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class LCLM_TelnetListener implements Listener
{
    public LCLM_TelnetListener()
    {
        if(!Bukkit.getPluginManager().isPluginEnabled("BukkitTelnet"))
        {
            Bukkit.broadcastMessage(ChatColor.RED + "BukkitTelnet cannot be found, disabling integration.");
            return;
        }
        Bukkit.getPluginManager().registerEvents(this, LynxCamLibertyMod.plugin);
    }
    
    @EventHandler
    public void onTelnetPreLoginEvent(TelnetPreLoginEvent event)
    {
        String ip = event.getIp();
        if(LCLM_Rank.isEqualOrHigher(LCLM_Rank.getRankFromIp(ip), LCLM_Rank.Rank.SUPER))
        {
            event.setBypassPassword(true);
            event.setName(ip + " [" + LCLM_Rank.getNameFromIp(ip) + "]");
            Bukkit.broadcastMessage(ChatColor.DARK_GREEN + LCLM_Rank.getNameFromIp(ip) + " logged in via telnet.");
        }
        else
        {
            event.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onTelnetCommand(TelnetCommandEvent event)
    {
        CommandSender player = event.getSender();
        FileConfiguration commands = LCLM_Configs.getCommands().getConfig();
        for (String blocked : commands.getConfigurationSection("").getKeys(false))
        {
            if (blocked.equalsIgnoreCase(event.getCommand().replaceAll("/", "")))
            {
                if (!LCLM_Rank.isRank(player, commands.getInt(blocked + ".rank")))
                {
                    player.sendMessage(ChatColor.RED + "You are not authorised to use this command.");
                    event.setCancelled(true);
                }
            }
        }
        for (Player player2 : Bukkit.getOnlinePlayers())
        {
            if (LCLM_Rank.isSpecialist(player2))
            {
                player2.sendMessage(ChatColor.GRAY + ChatColor.ITALIC.toString() + player.getName() + ": " + event.getCommand().toLowerCase());
            }
        }
    }
}
