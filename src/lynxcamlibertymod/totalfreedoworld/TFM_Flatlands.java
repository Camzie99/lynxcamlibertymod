package lynxcamlibertymod.totalfreedoworld;

import net.camtech.camutils.CUtils_Methods;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

public class TFM_Flatlands extends TFM_CustomWorld
{
    private static final String GENERATION_PARAMETERS = "16,stone,32,dirt,1,grass";
    private static final String WORLD_NAME = "flatlands";

    private TFM_Flatlands()
    {
    }

    @Override
    protected World generateWorld()
    {

        WorldCreator worldCreator = new WorldCreator(WORLD_NAME);
        worldCreator.generateStructures(false);
        worldCreator.type(WorldType.NORMAL);
        worldCreator.environment(World.Environment.NORMAL);
        worldCreator.generator(new CleanroomChunkGenerator(GENERATION_PARAMETERS));

        World world = Bukkit.getServer().createWorld(worldCreator);

        world.setSpawnFlags(false, false);
        world.setSpawnLocation(0, 50, 0);

        Block welcomeSignBlock = world.getBlockAt(0, 50, 0);
        welcomeSignBlock.setType(Material.SIGN_POST);
        org.bukkit.block.Sign welcomeSign = (org.bukkit.block.Sign) welcomeSignBlock.getState();

        org.bukkit.material.Sign signData = (org.bukkit.material.Sign) welcomeSign.getData();
        signData.setFacingDirection(BlockFace.NORTH);

        String randomChatColour = CUtils_Methods.colour("&-");
        welcomeSign.setLine(0, ChatColor.GOLD + "Welcome to");
        welcomeSign.setLine(1, randomChatColour + "---");
        welcomeSign.setLine(2, ChatColor.GREEN + "Flatlands");
        welcomeSign.setLine(3, randomChatColour + "---");
        welcomeSign.update();

        return world;
    }

    public static TFM_Flatlands getInstance()
    {
        return TFM_FlatlandsHolder.INSTANCE;
    }

    private static class TFM_FlatlandsHolder
    {
        private static final TFM_Flatlands INSTANCE = new TFM_Flatlands();
    }
}
