package lynxcamlibertymod;

import net.camtech.camutils.CUtils_Config;

public class LCLM_Configs
{

    public static CUtils_Config getAdmins()
    {
        return admins;
    }

    public static CUtils_Config getCommands()
    {
        return commands;
    }
    
    public static CUtils_Config getBans()
    {
        return bans;
    }
    
    public static CUtils_Config getPlayers()
    {
        return players;
    }

    public static CUtils_Config getMainConfig()
    {
        return mainconfig;
    }
    
    static CUtils_Config admins;
    static CUtils_Config players = admins;
    static CUtils_Config commands;
    static CUtils_Config bans;
    static CUtils_Config mainconfig;
    
    public LCLM_Configs()
    {
        admins = new CUtils_Config(LynxCamLibertyMod.plugin, "players.yml");
        admins.saveDefaultConfig();
        commands = new CUtils_Config(LynxCamLibertyMod.plugin, "commands.yml");
        commands.saveDefaultConfig();
        bans = new CUtils_Config(LynxCamLibertyMod.plugin, "bans.yml");
        bans.saveDefaultConfig();
        mainconfig = new CUtils_Config(LynxCamLibertyMod.plugin, "config.yml");
        mainconfig.saveDefaultConfig();
    }
}
