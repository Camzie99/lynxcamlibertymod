## Welcome to the LynxCamLibertyMod ##
The LynxCamLibertyMod is an all new type of freedom plugin, it takes the basic concepts and ideas of the TotalFreedomMod and executes them in different and more flexible ways. - Created and maintained by Camzie99.

Got any ideas for the LCLM or have you found a bug? Be sure to post an issue in our tracker.